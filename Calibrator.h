#pragma once
#ifndef CALIBRATOR_H
#define CALIBRATOR_H

#include <opencv2/imgcodecs.hpp>

#include <vector>

using namespace std;
using namespace cv;

class Calibrator
{
public:
	static bool CameraCalib(const vector<string>& imagelist, Size boardSize, float squareSize, string fileName, bool displayCorners = false, bool showRectified = true);
};
#endif
