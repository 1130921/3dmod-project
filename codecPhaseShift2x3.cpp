#include "CodecPhaseShift2x3.h"
#include <math.h>


#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

static unsigned int nPhases = 16;

// Encoder


// Cosine function vector (3-channel)
cv::Mat computePhaseVector(unsigned int length, float phase, float pitch){

	cv::Mat phaseVector(length, 1, CV_8UC3);

	const float pi = M_PI;

	// Loop through vector
	for (int i = 0; i<phaseVector.rows; i++){
		// Amplitude of channels
		float amp = 0.5*(1 + cos(2 * pi*i / pitch - phase));
		phaseVector.at<cv::Vec3b>(i, 0) = cv::Vec3b(255.0*amp, 255.0*amp, 255.0*amp);
	}

	return phaseVector;
}



EncoderPhaseShift2x3::EncoderPhaseShift2x3(unsigned int screenCols, unsigned int screenRows) : Encoder(screenCols, screenRows){

    // Set N
    N = 12;


    // Precompute encoded patterns
    const float pi = M_PI;


    // Horizontally encoding patterns
    for(unsigned int i=0; i<3; i++){
        float phase = 2.0*pi/3.0 * i;
        float pitch = (float)screenCols/(float)nPhases;
        cv::Mat patternI(1,1,CV_8U);
        patternI = computePhaseVector(screenCols, phase, pitch);
        patternI = patternI.t();
        patterns.push_back(patternI);
    }

    // Phase cue patterns
    for(unsigned int i=0; i<3; i++){
        float phase = 2.0*pi/3.0 * i;
        float pitch = screenCols;
        cv::Mat patternI;
        patternI = computePhaseVector(screenCols, phase, pitch);
        patternI = patternI.t();
        patterns.push_back(patternI);
    }
    
 
    // Precompute vertically encoding patterns
    for(unsigned int i=0; i<3; i++){
        float phase = 2.0*pi/3.0 * i;
        float pitch = (float)screenRows/(float)nPhases;
        cv::Mat patternI;
        patternI = computePhaseVector(screenRows, phase, pitch);
        patterns.push_back(patternI);
    }

    // Precompute vertically phase cue patterns
    for(unsigned int i=0; i<3; i++){
        float phase = 2.0*pi/3.0 * i;
        float pitch = screenRows;
        cv::Mat patternI;
        patternI = computePhaseVector(screenRows, phase, pitch);
        patterns.push_back(patternI);
    }
    
}

cv::Mat EncoderPhaseShift2x3::getEncodingPattern(unsigned int depth){
    return patterns[depth];
}

// Decoder
DecoderPhaseShift2x3::DecoderPhaseShift2x3(unsigned int screenCols, unsigned int screenRows) : Decoder(screenCols, screenRows){


    N = 12;


    frames.resize(N);
}

void DecoderPhaseShift2x3::setFrame(unsigned int depth, cv::Mat frame){
    frames[depth] = frame;
}

void DecoderPhaseShift2x3::decodeFrames(cv::Mat &up, cv::Mat &vp, cv::Mat &mask){

    // TODO

}
