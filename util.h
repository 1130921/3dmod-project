#ifndef UTIL_H
#define UTIL_H

#include <opencv2/opencv.hpp>
#include <vector>



namespace util {

	bool loadImages(std::string filename, std::vector<cv::Mat>& images);
	bool readStringList(std::string filename, std::vector<std::string>& l);
	void saveObj(std::string filename, std::vector<cv::Point3d>& points3D, std::vector<cv::Vec3b>& colors);
}



#endif