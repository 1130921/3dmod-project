#ifndef CodecGrayCode_H
#define CodecGrayCode_H

#include "Codec.h"
#include <opencv2/opencv.hpp>

class EncoderGrayCode : public Encoder {
    public:
        EncoderGrayCode(unsigned int screenCols, unsigned int screenRows, unsigned int depth);
        // Encoding
        cv::Mat getEncodingPattern(unsigned int depth);
    private:
        std::vector<cv::Mat> patterns;
		unsigned depth;
};

class DecoderGrayCode : public Decoder {
    public:
        DecoderGrayCode(unsigned int _screenCols, unsigned int _screenRows, unsigned int depth);
        // Decoding
        void setFrame(unsigned int depth, const cv::Mat frame);
        void decodeFrames(cv::Mat &up, cv::Mat &vp, cv::Mat &mask);
    private:
        std::vector<cv::Mat> frames;
		unsigned depth;
};

#endif // CodecGrayCode_H
