#include "util.h"
#include <iostream>
#include <vector>
#include <opencv2/opencv.hpp>

using namespace cv;

namespace util {
	bool loadImages(std::string filename, std::vector<cv::Mat>& images) {
		images.clear();
		FileStorage fs(filename, FileStorage::READ);
		if (!fs.isOpened())
			return false;
		FileNode n = fs.getFirstTopLevelNode();
		if (n.type() != FileNode::SEQ)
			return false;
		FileNodeIterator it = n.begin(), it_end = n.end();
		for (; it != it_end; ++it) {
			cv::Mat img = cv::imread((std::string)*it);
			if (img.empty()) {
				std::cout << "Failed to load " << (std::string)*it << std::endl;
				return false;
			}
			else 
				images.emplace_back(img);
			
		}
		return true;
	}

	bool readStringList(std::string filename, std::vector<std::string>& l)
	{
		l.clear();
		FileStorage fs(filename, FileStorage::READ);
		if (!fs.isOpened())
			return false;
		FileNode n = fs.getFirstTopLevelNode();
		if (n.type() != FileNode::SEQ)
			return false;
		FileNodeIterator it = n.begin(), it_end = n.end();
		for (; it != it_end; ++it) {
			l.push_back((std::string)*it);
		}
		return true;
	}



	void saveObj(std::string filename, std::vector<Point3d>& points3D, std::vector<Vec3b>& colors) {
		std::ofstream fstr(filename);
		if (!fstr.is_open())
			return;


		for (unsigned int i = 0; i < points3D.size(); ++i) {

			//Vector3f& vn = normals[i];
			//fstr << "vn " << vn[0] << " " << vn[1] << " " << vn[2] << std::endl;
			fstr << "v " << points3D[i].x << " " << points3D[i].y << " " << points3D[i].z << " " << colors[i].val[2] / 255.0f << " " << colors[i].val[1] / 255.0f << " " << colors[i].val[0] / 255.0f << std::endl;
		}

		fstr.close();
	}

}