#ifndef CODECPHASESHIFT2X3_H
#define CODECPHASESHIFT2X3_H

#include "Codec.h"
#include <opencv2/opencv.hpp>

class EncoderPhaseShift2x3 : public Encoder {
    public:
        EncoderPhaseShift2x3(unsigned int screenCols, unsigned int screenRows);
        // Encoding
        cv::Mat getEncodingPattern(unsigned int depth);
    private:
        std::vector<cv::Mat> patterns;
};

class DecoderPhaseShift2x3 : public Decoder {
    public:
        DecoderPhaseShift2x3(unsigned int screenCols, unsigned int screenRows);
        // Decoding
        void setFrame(unsigned int depth, cv::Mat frame);
        void decodeFrames(cv::Mat &up, cv::Mat &vp, cv::Mat &mask);
    private:
        std::vector<cv::Mat> frames;
};

#endif // CODECPHASESHIFT2X3_H
