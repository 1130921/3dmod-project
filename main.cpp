#include "codecPhaseShift2x3.h"
#include "codecGrayCode.h"
#include "util.h"
#include "Calibrator.h"

#include <opencv2/opencv.hpp>
#include <opencv2/rgbd.hpp>
#include "eigen3/Eigen/Core"
#include "eigen3/Eigen/Dense"

#include <opencv2/phase_unwrapping.hpp>

#include "omp.h"
#include <random>


using namespace cv;
using namespace std;

#define ESC_KEY 27

#define DIR_HOR 1
#define DIR_VER 2




struct View {
	std::string config; // config file consists of images to load
	std::vector<cv::Mat> frames;
};


bool highRes = false;

//*******************************************************************
// ENCODE PATTERNS AND VISUALISE
//*******************************************************************
void showGrayCodes()
{
	// set resolution of the projected patterns
	unsigned patternWidth = 1920;
	unsigned patternHeight = 1080;

	Encoder* encoder = new EncoderGrayCode(patternWidth, patternHeight, 10);


	namedWindow("Gray Code Pattern", WINDOW_NORMAL);
	resizeWindow("Gray Code Pattern", patternWidth / 2, patternHeight / 2);
	for (unsigned n = 0; n < encoder->getNPatterns(); ++n) {

		cv::Mat pattern = encoder->getEncodingPattern(n);

		// resize the 1D pattern to 2D
		cv::Mat dummy = pattern.clone();
		cv::resize(dummy, pattern, cv::Size(patternWidth, patternHeight));

		imshow("Gray Code Pattern", pattern);

		// wait for key input if waitKey(0) or wait x milliseconds when waitKey(x)
		char key = waitKey(1000);
		if (key == ESC_KEY)
			break;

	}

	cv::destroyWindow("Gray Code Pattern");
	delete encoder;

}


void showGrayCodesInputImages()
{
	// set resolution of the images
	unsigned width = 1200;
	unsigned height = 800;

	// Create two views
	std::vector<View> views;
	views.resize(2);

	// Load Gray Code Images
	views[0].config = "../dataset/GrayCodes/graycodes_view0.xml";
	views[1].config = "../dataset/GrayCodes/graycodes_view1.xml";


	//*******************************************************************
	// Load images for all views
	//*******************************************************************

	for (View& view : views)
		if (!util::loadImages(view.config, view.frames)) {
			std::cout << "Failed to load images from " << view.config << std::endl;
			return;
		}


	namedWindow("Gray Codes Image View Left", WINDOW_NORMAL);
	resizeWindow("Gray Codes Image View Left", width / 2, height / 2);
	namedWindow("Gray Codes Image View Right", WINDOW_NORMAL);
	resizeWindow("Gray Codes Image View Right", width / 2, height / 2);
	for (unsigned v = 0; v < views[0].frames.size(); ++v) {

		imshow("Gray Codes Image View Left", views[0].frames[v]);
		imshow("Gray Codes Image View Right", views[1].frames[v]);

		// wait for key input if waitKey(0 or wait x milliseconds when waitKey(x)
		char key = waitKey(1000);
		if (key == ESC_KEY)
			break;

	}

	cv::destroyWindow("Gray Codes Image View Left");
	cv::destroyWindow("Gray Codes Image View Right");
}


//*******************************************************************
// ENCODE PATTERNS AND VISUALISE
//*******************************************************************
void showSinusPatterns()
{
	// set resolution of the projected patterns
	unsigned patternWidth = 1920;
	unsigned patternHeight = 1080;
	Encoder* encoder = new EncoderPhaseShift2x3(patternWidth, patternHeight);


	namedWindow("Sinus Pattern", WINDOW_NORMAL);
	resizeWindow("Sinus Pattern", patternWidth / 2, patternHeight / 2);
	for (unsigned n = 0; n < encoder->getNPatterns(); ++n) {

		cv::Mat pattern = encoder->getEncodingPattern(n);

		// resize the 1D pattern to 2D
		cv::Mat dummy = pattern.clone();
		cv::resize(dummy, pattern, cv::Size(patternWidth, patternHeight));

		imshow("Sinus Pattern", pattern);

		// wait for key input if waitKey(0) or wait x milliseconds when waitKey(x)
		char key = waitKey(1000);
		if (key == ESC_KEY)
			break;

	}

	cv::destroyWindow("Sinus Pattern");

	delete encoder;

}

void showSinusInputImages()
{
	// set resolution of the images
	unsigned width = 1200;
	unsigned height = 800;

	// Create two views
	std::vector<View> views;
	views.resize(2);

	// Load Sinus Images
	views[0].config = "../dataset/Sinus/sinus_view0.xml";
	views[1].config = "../dataset/Sinus/sinus_view1.xml";


	//*******************************************************************
	// Load images for all views
	//*******************************************************************

	for (View& view : views)
		if (!util::loadImages(view.config, view.frames)) {
			std::cout << "Failed to load images from " << view.config << std::endl;
			return;
		}

	namedWindow("Sinus Image View Left", WINDOW_NORMAL);
	resizeWindow("Sinus Image View Left", width / 2, height / 2);
	namedWindow("Sinus Image View Right", WINDOW_NORMAL);
	resizeWindow("Sinus Image View Right", width / 2, height / 2);
	for (unsigned v = 0; v < views[0].frames.size(); ++v) {

		imshow("Sinus Image View Left", views[0].frames[v]);
		imshow("Sinus Image View Right", views[1].frames[v]);

		// wait for key input if waitKey(0 or wait x milliseconds when waitKey(x)
		char key = waitKey(1000);
		if (key == ESC_KEY)
			break;

	}

	cv::destroyWindow("Sinus Image View Left");
	cv::destroyWindow("Sinus Image View Right");
}

void printMinMax(Mat mat, double &min_ref, double &max_ref)
{

	double min, max;
	cv::minMaxLoc(mat, &min, &max);
	std::cout << "Min: " << min << ", Max: " << max << std::endl;
	min_ref = min;
	max_ref = max;
}

cv::Mat calculateIntensity(Mat i1, Mat i2, Mat i3)
{
	// Convert all images to gray-scale
	cvtColor(i1, i1, COLOR_BGR2GRAY);
	cvtColor(i2, i2, COLOR_BGR2GRAY);
	cvtColor(i3, i3, COLOR_BGR2GRAY);
	// Calculate intensity texture
	Mat intensity = (i1 + i2 + i3) / (3.0f);
	return intensity;
}


cv::Mat thresholdIntensity(Mat intensity)
{
	Mat thresholded;
	cv::threshold(intensity, thresholded, 20.0, 255.0, cv::THRESH_BINARY);
	thresholded.convertTo(thresholded, CV_32F);
	return thresholded / 255.0f;
}

cv::Mat calculatePhase(Mat i1, Mat i2, Mat i3)
{
	// Convert all images to gray-scale
	cvtColor(i1, i1, COLOR_BGR2GRAY);
	cvtColor(i2, i2, COLOR_BGR2GRAY);
	cvtColor(i3, i3, COLOR_BGR2GRAY);
	// Convert to float format
	i1.convertTo(i1, CV_32F);
	i2.convertTo(i2, CV_32F);
	i3.convertTo(i3, CV_32F);

	// For output
	Mat output(i1.rows, i1.cols, CV_32F);

	// Calculate phase
	phase(2.0*i1 - i3 - i2, sqrt(3.0)*(i2 - i3), output);
	// Map to screen-space
	return output;
}

cv::Mat unwrap(const cv::Mat highFreq, const cv::Mat lowFreq, int dir) {
	// Scale low freq with 16 -> amount of phases
	cv::Mat k = (16.0f * lowFreq - highFreq);

	// Scale k back to phase
	k = k / (2 * M_PI);

	// Use high freq + factor times period  (2pi, 4pi, 6pi, .... 32pi)
	cv::Mat upUnwrapped = (highFreq + k * 2 * M_PI);

	// Scale back down to phase space
	return upUnwrapped / 16.0F;
}

void debugSinusPatterns()
{
	// set resolution of the projected patterns
	unsigned patternWidth = 1920;
	unsigned patternHeight = 1080;
	Encoder* encoder = new EncoderPhaseShift2x3(patternWidth, patternHeight);

	std::vector<Mat> patterns;


	namedWindow("Sinus Pattern", WINDOW_NORMAL);
	resizeWindow("Sinus Pattern", patternWidth / 2, patternHeight / 2);
	for (unsigned n = 0; n < encoder->getNPatterns(); ++n) {

		cv::Mat pattern = encoder->getEncodingPattern(n);

		// resize the 1D pattern to 2D
		cv::Mat dummy = pattern.clone();
		cv::resize(dummy, pattern, cv::Size(patternWidth, patternHeight));

		patterns.push_back(pattern.clone());
	}

	// Horizontal | matrices are in phase-space
	Mat HorizontalHighFreq = calculatePhase(patterns[0], patterns[1], patterns[2]);
	Mat HorizontalLowFreq = calculatePhase(patterns[3], patterns[4], patterns[5]);

	// Vertical | matrices are in phase-space
	Mat VerticallHighFreq = calculatePhase(patterns[6], patterns[7], patterns[8]);
	Mat VerticalLowFreq = calculatePhase(patterns[9], patterns[10], patterns[11]);

	//// Unwrap | matrices are in phase-space
	Mat HorizontalUnwrapped = unwrap(HorizontalHighFreq, HorizontalLowFreq, DIR_HOR);
	Mat VerticalUnwrapped = unwrap(VerticallHighFreq, VerticalLowFreq, DIR_VER);

	// Same size as all the other images, fully black
	Mat black(HorizontalUnwrapped.rows, HorizontalUnwrapped.cols, HorizontalUnwrapped.type(), Scalar(0, 0, 0));
	// Combine Matrices: color will be unique index -> RGB (horizontal-coordinate, vertical-coordinate, 0) = unique ID for a pixel
	std::vector<cv::Mat> BGRArray;
	// Nothing on the blue channel
	BGRArray.push_back(black);
	// Green channel = vertical
	BGRArray.push_back(HorizontalUnwrapped / (2 * M_PI));
	// Red channel = horizontal
	BGRArray.push_back(VerticalUnwrapped / (2 * M_PI));

	cv::Mat color;
	cv::merge(BGRArray, color);

	char key = waitKey();
	if (key == ESC_KEY)
		return;

	//cv::destroyWindow("Sinus Pattern");

	delete encoder;
}

Mat calculatePhases(View views, std::string title, Mat &horitzontal, Mat &vertical)
{
	Mat HorizontalHighFreq = calculatePhase(views.frames[0], views.frames[1], views.frames[2]);
	Mat HorizontalLowFreq = calculatePhase(views.frames[3], views.frames[4], views.frames[5]);

	// Vertical | matrices are in phase-space
	Mat VerticallHighFreq = calculatePhase(views.frames[6], views.frames[7], views.frames[8]);
	Mat VerticalLowFreq = calculatePhase(views.frames[9], views.frames[10], views.frames[11]);

	Mat intensity = calculateIntensity(views.frames[3], views.frames[4], views.frames[5]);
	Mat mask = thresholdIntensity(intensity);

	HorizontalHighFreq = (HorizontalHighFreq).mul(mask);
	HorizontalLowFreq = (HorizontalLowFreq).mul(mask);
	VerticallHighFreq = (VerticallHighFreq).mul(mask);
	VerticalLowFreq = (VerticalLowFreq).mul(mask);


	//	imshow("Mask", mask / (2 * M_PI));

	// Unwrap | matrices are in phase-space
	Mat HorizontalUnwrapped = unwrap(HorizontalHighFreq, HorizontalLowFreq, DIR_HOR);
	Mat VerticalUnwrapped = unwrap(VerticallHighFreq, VerticalLowFreq, DIR_VER);

	// Same size as all the other images, fully black
	Mat black(HorizontalUnwrapped.rows, HorizontalUnwrapped.cols, HorizontalUnwrapped.type(), Vec3b(0.0f, 0.0f, 0.0f));
	// Combine Matrices: color will be unique index -> RGB (horizontal-coordinate, vertical-coordinate, 0) = unique ID for a pixel
	std::vector<cv::Mat> BGRArray;
	// Nothing on the blue channel
	BGRArray.push_back(black);
	// Green channel = vertical
	BGRArray.push_back(HorizontalUnwrapped / (2 * M_PI));
	// Red channel = horizontal
	BGRArray.push_back(VerticalUnwrapped / (2 * M_PI));

	horitzontal = HorizontalUnwrapped / (2 * M_PI);
	vertical = VerticalUnwrapped / (2 * M_PI);

	cv::Mat color;
	cv::merge(BGRArray, color);
	//	imshow("color", color);


	// Output | convert to screen-space
	//imshow("Horizontal - High Frequency", HorizontalHighFreq / (2 * M_PI));
	//imshow("Horizontal - Low Frequency", HorizontalLowFreq / (2 * M_PI));
	//imshow("Vertical - High Frequency", VerticallHighFreq / (2 * M_PI));
	//imshow("Vertical - Low Frequency", VerticalLowFreq / (2 * M_PI));
	// Output | matrices are in phase-space
//	imshow("Horizontal - Unwrapped", HorizontalUnwrapped / (2 * M_PI));
//	imshow("Vertical - Unwrapped", VerticalUnwrapped / (2 * M_PI));
//	imshow("Combined - Colormap - View " + title, color);
	return color;
}

static int calibrateCamera(bool displayCorners, bool showRectified)
{
	const Size boardSize(9, 7); //(width,height)
	float squareSize = 1.0f;

	printf("Board size w: %d, h: %d; Square size: %.2f\n", boardSize.width, boardSize.height, squareSize);

	string chessImagelistfn;
	if(!highRes)
		chessImagelistfn = "../dataset/Sinus/sinus_chess.xml";
	else
		chessImagelistfn = "../dataset/Sinus_HighRes/sinus_chess.xml";

	vector<string> chessImagelist;


	if (!util::readStringList(chessImagelistfn, chessImagelist)) {
		std::cout << "Failed to load images from " << chessImagelistfn << std::endl;
		return 0;
	}

	bool ok;

	string outputFile = highRes ? "intrinsicsbig.yml" : "intrinsicssmall.yml";
	ok = Calibrator::CameraCalib(chessImagelist, boardSize, squareSize, outputFile, displayCorners, showRectified);

	return ok;
}

void rectifyImages(View view, Mat M1, Mat D1)
{
	for (unsigned v = 0; v < view.frames.size(); ++v) {

		Mat sourceImage = view.frames[v];
		Mat rectifiedImage;
		Mat distMap1;
		Mat distMap2;

		// Calculate optimalNewCameraMatrix givin intrinsics and source image
		Mat optimalCamMatrix = getOptimalNewCameraMatrix(M1, D1, sourceImage.size(), 1, sourceImage.size(), 0);

		// Undistort image
		initUndistortRectifyMap(M1, D1, Mat(), optimalCamMatrix, sourceImage.size(), CV_32F, distMap1, distMap2);

		// Remap original image using previous result
		remap(sourceImage, rectifiedImage, distMap1, distMap2, 1);

		// Replace source image with rectified image
		view.frames[v] = rectifiedImage;
	}
}

bool floatDifference(float f1, float f2, float eps = 0.01f)
{
	return (fabs(f1 - f2) < eps);
}

struct Point2fLess
{
	bool operator()(Point2f const&lhs, Point2f const& rhs) const
	{
		return floatDifference(lhs.x, rhs.x, 0.004f) ? lhs.y < rhs.y : lhs.x < rhs.x;
	}
};


void findCorrespondences2(Mat left, Mat right, Mat left_horizontal, Mat left_vertical, Mat right_horizontal, Mat right_vertical, std::vector<Point2f> &pointsLeft, std::vector<Point2f> &pointsRight)
{

	std::map<Point2f, Point2i, Point2fLess> right_map;


	std::cout << "Mapping right image" << std::endl;
	for (int i = 0; i < right_horizontal.rows; i++) {
		for (int j = 0; j < right_horizontal.cols; j++)
		{
			Point2f key = Point2f(right_horizontal.at<float>(i, j), right_vertical.at<float>(i, j));
			right_map[key] = Point2i(j, i);
		}
	}
	std::cout << "Looking for corespondences" << std::endl;


	std::vector<Point2f> leftCorespondences;
	std::vector<Point2f> rightCorespondences;


	for (int i = 0; i < right_horizontal.rows; i++) {


		for (int j = 0; j < right_horizontal.cols; j++)
		{
			int row = i;
			int col = j;


			float left_horizontal_value = left_horizontal.at<float>(row, col);
			float left_vertical_value = left_vertical.at<float>(row, col);

			if (floatDifference(left_horizontal_value, 0) && floatDifference(left_vertical_value, 0))
				continue;


			bool correspondanceFound = false;
			Point2f key = Point2f(left_horizontal_value, left_vertical_value);
			if (right_map.find(key) != right_map.end())
			{
				Point2i leftMatch(col, row);
				Point2i rightMatch = right_map[key];

				if (fabs(leftMatch.x - rightMatch.x) > (left_horizontal.cols / 2))
					continue;
				if (fabs(leftMatch.y - rightMatch.y) > (left_horizontal.rows / 2))
					continue;

				leftCorespondences.push_back(leftMatch);
				rightCorespondences.push_back(rightMatch);
				correspondanceFound = true;

				//std::cout << "Match " << i << " Found" << std::endl;
				//std::cout << "\t" << leftMatch.x << "," << leftMatch.y << std::endl;
				//std::cout << "\t" << rightMatch.x << "," << rightMatch.y << std::endl;
			}
		}
	}

	std::cout << "Total Correspondences: " << leftCorespondences.size() << endl;
	std::cout << "=====================================================" << std::endl;

	pointsLeft = leftCorespondences;
	pointsRight = rightCorespondences;
}

void writeMesh(Mat points)
{
	ofstream myfile;
	myfile.open("pointcloud.obj");

	for (int i = 0; i < points.cols; i++)
	{

		myfile << "v ";
		myfile << points.at<float>(0, i) / points.at<float>(3, i);
		myfile << " ";
		myfile << points.at<float>(1, i) / points.at<float>(3, i);
		myfile << " ";
		myfile << points.at<float>(2, i) / points.at<float>(3, i);
		myfile << std::endl;
	}
	myfile.close();
}


Mat createProjectionMatrix(Mat R, Mat T, Mat cameraMatrix)
{
	// 3 x 4 projection matrix
	Mat projection = Mat(3, 4, CV_64F);
	// Construct [R | T]
	Mat projection_array[] = {
		R,
		T
	};
	// Concat
	hconcat(projection_array, 2, projection);
	// Multiply cameraMatrix * [R | T]
	projection = cameraMatrix * projection;
	return projection;
}

Mat homogenousToEuclidean(Mat triagulatedPoints)
{
	vector<Mat> splitted = {
		triagulatedPoints.row(0) / triagulatedPoints.row(3),
		triagulatedPoints.row(1) / triagulatedPoints.row(3),
		triagulatedPoints.row(2) / triagulatedPoints.row(3)
	};

	merge(splitted, triagulatedPoints);
	return triagulatedPoints;
}

Mat calculateDepthMap(Mat triangulatedPoints, std::vector<Point2f> coords, int rows, int cols, double &max)
{
	// Create output as big as original
	Mat depthMap(rows, cols, CV_64F);

	max = 0;

	// For every 3D point
	for (int i = 0; i < triangulatedPoints.cols; i++)
	{
		Vec3f a = triangulatedPoints.at<Vec3f>(0, i);
		Vec3f b = Vec3f(0, 0, 0);
		// Distance between 3D point and 0,0,0
		double depth = norm(a - b);

		// Save maximum depth to normalize later on
		if (depth > max)
			max = depth;

		// At which position should we save this depth-value?
		Point2i coord = coords[i];
		depthMap.at<double>(coord.y, coord.x) = depth;
	}

	return depthMap;
}

void writeMatrixToFile(string fileName, string matrixName, Mat matrix)
{
	FileStorage fs(fileName, FileStorage::WRITE);
	if (fs.isOpened())
	{
		fs << matrixName << matrix;
		fs.release();
	}
	else
		cerr << "Error: can not save " << fileName << std::endl;
}

bool readMatrixFromFile(string fileName, string matrixName, Mat &out)
{
	FileStorage fs(fileName, FileStorage::READ);
	if (!fs.isOpened())
	{
		cerr << "Error: can not open " << fileName << std::endl;
		return false;
	}

	fs[matrixName] >> out;
	fs.release();
	return true;
}


bool in_pov = false;
cv::viz::Viz3d viewer = cv::viz::Viz3d("Point Cloud");

void  KeyboardViz3d(const viz::KeyboardEvent &w, void *t)
{
	viz::Viz3d *fen = (viz::Viz3d*)t;
	if (w.action)
	{
		cout << "you pressed " << w.code << " = " << w.symbol << " in viz window " << fen->getWindowName() << "\n";

		if (w.code == 'l')
		{
			std::cout << "L pressed" << std::endl;
		}

	}

}


int main(int argc, char** argv)
{

	//showSinusPatterns();
	//showSinusInputImages();

	//showGrayCodes();
	//showGrayCodesInputImages();


	// Create two views
	std::vector<View> views;
	views.resize(2);

	cv::Mat img1;
	cv::Mat img2;

	// Load Sinus Images
	if (!highRes) {
		views[0].config = "../dataset/Sinus/sinus_view0.xml";
		views[1].config = "../dataset/Sinus/sinus_view1.xml";
		// Image for viz cam
		img1 = cv::imread("../dataset/Sinus/view0/00.jpg");
		img2 = cv::imread("../dataset/Sinus/view1/00.jpg");
	}
	else
	{
		views[0].config = "../dataset/Sinus_HighRes/sinus_view0.xml";
		views[1].config = "../dataset/Sinus_HighRes/sinus_view1.xml";
		// Image for viz cam
		img1 = cv::imread("../dataset/Sinus_HighRes/view0/00.jpg");
		img2 = cv::imread("../dataset/Sinus_HighRes/view1/00.jpg");
	}


	//*******************************************************************
	// Load images for all views
	//*******************************************************************

	for (View& view : views)
		if (!util::loadImages(view.config, view.frames)) {
			std::cout << "Failed to load images from " << view.config << std::endl;
			return 0;
		}

	//debugSinusPatterns();

	//*******************************************************************
	// Calibrate camera
	//*******************************************************************

	Mat M1, D1;
	string intrinsic_filename = highRes ? "intrinsicsbig.yml" : "intrinsicssmall.yml";

	// Try to open file
	FileStorage intrinsicsfs(intrinsic_filename, FileStorage::READ);
	// If it doesn't exist, start calibration
	if (!intrinsicsfs.isOpened())
	{
		std::cout << "Intrinsics not found, starting calibration" << std::endl;
		calibrateCamera(false, false);
	}

	FileStorage fs(intrinsic_filename, FileStorage::READ);
	if (fs.isOpened())
	{
		fs["M1"] >> M1;
		fs["D1"] >> D1;
		fs.release();
	}

	std::cout << "Cam Matrix:" << std::endl;
	std::cout << M1 << std::endl;
	std::cout << "=====================================================" << std::endl;


	//*******************************************************************
	// Rectify source images
	//*******************************************************************

	rectifyImages(views[0], M1, D1);
	rectifyImages(views[1], M1, D1);

	//*******************************************************************
	// Unwrap Phases
	//*******************************************************************

	Mat left_horizontal;
	Mat left_vertical;

	Mat right_horizontal;
	Mat right_vertical;

	Mat left = calculatePhases(views[0], "Left", left_horizontal, left_vertical);
	Mat right = calculatePhases(views[1], "Right", right_horizontal, right_vertical);


	//*******************************************************************
	// Search for correspondences
	//*******************************************************************

	std::vector<Point2f> pointsLeft;
	std::vector<Point2f> pointsRight;
	findCorrespondences2(left, right, left_horizontal, left_vertical, right_horizontal, right_vertical, pointsLeft, pointsRight);

	// Draw correspondances
	for (int i = 0; i < pointsLeft.size(); i++)
	{
		cv::circle(left, pointsLeft[i], 3, Vec3f(255.0, 255.0, 255.0));
		cv::circle(right, pointsRight[i], 3, Vec3f(255.0, 255.0, 255.0));
	}
	namedWindow("Left View", WINDOW_NORMAL);
	resizeWindow("Left View", 1200, 800);
	imshow("Left View", left);


	namedWindow("Right View", WINDOW_NORMAL);
	resizeWindow("Right View", 1200, 800);
	imshow("Right View", right);


	//*******************************************************************
	// Essential Matrix
	//*******************************************************************

	Mat essentialMat;
	
	// If we are using low res and we have not saved the essential matrix to file yet -> recalc
	if (!highRes && !readMatrixFromFile("essentialsmall.yml", "essentialMat", essentialMat))
	{
		essentialMat = findEssentialMat(pointsLeft, pointsRight, M1, RANSAC, 0.99, 1.0);
		writeMatrixToFile("essentialsmall.yml", "essentialMat", essentialMat);
	}
	// If we are using high res and we have not saved the eseential matrix to file yet -> recalc
	else if (highRes && !readMatrixFromFile("essentialbig.yml", "essentialMat", essentialMat))
	{
		essentialMat = findEssentialMat(pointsLeft, pointsRight, M1, RANSAC, 0.99, 1.0);
		writeMatrixToFile("essentialbig.yml", "essentialMat", essentialMat);
	}


	//*******************************************************************
	// Recover Pose
	//*******************************************************************

	Mat r;
	Mat t;
	recoverPose(essentialMat, pointsLeft, pointsRight, M1, r, t);

	//*******************************************************************
	// Triangulate
	//*******************************************************************

	Mat identity_3 = Mat::eye(3, 3, CV_64F);
	Mat translation_1 = Mat::zeros(3, 1, CV_64F);
	Mat projection_camera_1 = createProjectionMatrix(identity_3, translation_1, M1);
	Mat projection_camera_2 = createProjectionMatrix(r, t, M1);

	// Triangulate the points
	Mat triangulatedPoints;
	triangulatePoints(projection_camera_1, projection_camera_2, pointsLeft, pointsRight, triangulatedPoints);

	// Write mesh as OBJ to import into meshlab
	writeMesh(triangulatedPoints);

	// Convert from 4D to 3D
	triangulatedPoints = homogenousToEuclidean(triangulatedPoints);


	//*******************************************************************
	// Point Cloud
	//*******************************************************************

	bool camera_pov = false;

	viewer.registerKeyboardCallback(KeyboardViz3d, &viewer);

	cv::Matx33d M33((double*)M1.ptr());
	cv::Affine3d cam1_pose(identity_3, translation_1);
	Mat r_world = r.t();
	Mat t_world = r.t() * -t;
	cv::Affine3d cam2_pose(r_world, t_world);

	if (!camera_pov)
	{
		viz::WCameraPosition cam1(M33, img1);
		viewer.showWidget("Camera1", cam1);
		viewer.setWidgetPose("Camera1", cam1_pose);


		viz::WCameraPosition cam2(M33, img2);
		viewer.showWidget("Camera2", cam2);
		viewer.setWidgetPose("Camera2", cam2_pose);
	}

	Mat original;
	if(!highRes)
		original = imread("../dataset/Sinus/view0/00.jpg");
	else 
		original = imread("../dataset/Sinus_HighRes/view0/00.jpg");

	std::vector<Vec3b> colors_array;

	for (int i = 0; i < pointsLeft.size(); i++)
	{
		Point2i coord = pointsLeft[i];
		colors_array.push_back(original.at<Vec3b>(coord.y, coord.x));
	}


	cv::viz::WCloud cloud(triangulatedPoints, colors_array);

	viewer.showWidget("Cloud", cloud);

	if (camera_pov)
		viewer.setViewerPose(cam1_pose);
	else {
		//Mat transform_z = (Mat_<float>(3, 1) << 0.5f, 0, -1.5f);
		//viewer.setViewerPose(cv::Affine3d(identity_3, transform_z));
	}


	viewer.spinOnce();


	//*******************************************************************
	// Depth Map
	//*******************************************************************
	double max;
	Mat depthMap = calculateDepthMap(triangulatedPoints, pointsLeft, left.rows, left.cols, max);

	namedWindow("Depth Map", WINDOW_NORMAL);
	resizeWindow("Depth Map", 1200, 800);
	// Normalize to [0,1]
	imshow("Depth Map", depthMap / max);

	//*******************************************************************
	// Image Warping (Niet werkend)
	//*******************************************************************
	
	//Mat depth;
	//cv::rgbd::depthTo3d(depthMap, M1, depth);
	//depth.convertTo(depth, CV_64F);

	//std::cout << "Depth Map type: " << depthMap.type() << ", Dept type: " << depth.type() << endl;

//	Mat mask = Mat::zeros(original.rows, original.cols, CV_8UC1);;
//	Mat warpedImg;
//
//	Eigen::Quaterniond startRotation = (double*)identity_3.ptr();
//	Eigen::Quaterniond currentRotation = startRotation;
//	Eigen::Quaterniond endRotation = (double*)r.ptr();
//
//	Eigen::Quaterniond startTranslation = (double*)translation_1.ptr();
//	Eigen::Quaterniond currentTranslation = startTranslation;
//	Eigen::Quaterniond endTranslation = (double*)t.ptr();
//
//
//	int start = 0;
//	int end = 10;
//	int current = start;
//
//	while (current <= end)
//	{
//		currentRotation = startRotation.slerp((double)current / end, endRotation);
//		currentTranslation = startTranslation.slerp((double)current / end, endTranslation);
//
//		Mat interpRotation = r.clone();
//		Eigen::Matrix3d rot = currentRotation.toRotationMatrix();
//		interpRotation.at<double>(0, 0) = (double)rot(0, 0);
//		interpRotation.at<double>(0, 1) = (double)rot(0, 1);
//		interpRotation.at<double>(0, 2) = (double)rot(0, 2);
//
//		interpRotation.at<double>(1, 0) = (double)rot(1, 0);
//		interpRotation.at<double>(1, 1) = (double)rot(1, 1);
//		interpRotation.at<double>(1, 2) = (double)rot(1, 2);
//
//		interpRotation.at<double>(2, 0) = (double)rot(2, 0);
//		interpRotation.at<double>(2, 1) = (double)rot(2, 1);
//		interpRotation.at<double>(2, 2) = (double)rot(2, 2);
//
//		Mat interpTranslation = t.clone();
//		Eigen::Matrix3d trans = currentTranslation.toRotationMatrix();
//		interpTranslation.at<double>(0, 0) = (double)trans(0, 0);
//		interpTranslation.at<double>(1, 0) = (double)trans(1, 0);
//		interpTranslation.at<double>(2, 0) = (double)trans(2, 0);
//
//
//		Mat projection;
//		// Construct [R | T]
//		Mat projection_array[] = {
//			interpRotation,
//			interpTranslation
//		};
//		// Concat
//		hconcat(projection_array, 2, projection);
//
//		Mat projection_float;
//		projection.convertTo(projection_float, CV_32F);
//
//		Mat depthMap_float;
//		depthMap.convertTo(depthMap_float, CV_32F);
//
//		Mat M1_float;
//		M1.convertTo(M1_float, CV_32F);
//
//		Mat D1_float;
//		D1.convertTo(D1_float, CV_32F);
//
//		Mat orig;
//		original.convertTo(orig, CV_8UC1);
//
//		cv::rgbd::warpFrame(orig, depthMap_float, mask, projection_float, M1_float, D1_float, warpedImg);
//		imshow("Warped Img", warpedImg);
//
//		current++;
//		char key = waitKey(0);
//	}


	// wait for key input if waitKey(0 or wait x milliseconds when waitKey(x)
	char key = waitKey();
	if (key == ESC_KEY)
		return 0;

	return 0;
}





