#include "Calibrator.h"

#include <opencv2/calib3d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <iostream>

static void calcBoardCornerPositions(Size boardSize, float squareSize, vector<Point3f>& corners)
{
	corners.clear();
	for (int i = 0; i < boardSize.height; ++i)
		for (int j = 0; j < boardSize.width; ++j)
			corners.push_back(Point3f(j*squareSize, i*squareSize, 0));

}

static double computeReprojectionErrors(const vector<vector<Point3f> >& objectPoints,
	const vector<vector<Point2f> >& imagePoints,
	const vector<Mat>& rvecs, const vector<Mat>& tvecs,
	const Mat& cameraMatrix, const Mat& distCoeffs,
	vector<float>& perViewErrors)
{
	vector<Point2f> imagePoints2;
	size_t totalPoints = 0;
	double totalErr = 0, err;
	perViewErrors.resize(objectPoints.size());

	for (size_t i = 0; i < objectPoints.size(); ++i)
	{

		projectPoints(objectPoints[i], rvecs[i], tvecs[i], cameraMatrix, distCoeffs, imagePoints2);

		err = norm(imagePoints[i], imagePoints2, NORM_L2);

		size_t n = objectPoints[i].size();
		perViewErrors[i] = (float)std::sqrt(err*err / n);
		totalErr += err*err;
		totalPoints += n;
	}

	return std::sqrt(totalErr / totalPoints);
}

bool Calibrator::CameraCalib(const vector<string>& imageList, Size boardSize, float squareSize, string fileName, bool displayCorners, bool showRectified)
{
	vector<vector<Point2f> > imagePoints;
	Mat cameraMatrix, distCoeffs;
	Size imageSize;
	int chessBoardFlags = CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE;

	for (int i = 0; i < imageList.size(); i++)
	{
		const string& filename = imageList[i];
		Mat img = imread(filename, 0);

		if (img.empty())
			break;
		if (imageSize == Size())
			imageSize = img.size();


		bool found = false;
		vector<Point2f> pointBuf;

		found = findChessboardCorners(img, boardSize, pointBuf, chessBoardFlags);

		if (found)
		{
			cornerSubPix(img, pointBuf, Size(11, 11), Size(-1, -1), TermCriteria(TermCriteria::EPS + TermCriteria::COUNT, 30, 0.1));
			imagePoints.push_back(pointBuf);
		}


		if (displayCorners)
		{
			cout << filename << endl;
			Mat cimg;
			cvtColor(img, cimg, COLOR_GRAY2BGR);
			drawChessboardCorners(cimg, boardSize, Mat(pointBuf), found);
			imshow("corners", cimg);
			char c = (char)waitKey(500);
			if (c == 27 || c == 'q' || c == 'Q') //Allow ESC to quit
				exit(-1);
		}
		else
			putchar('.');

	}
	putchar('\n');

	
	cameraMatrix = Mat::eye(3, 3, CV_64F);
	//	cameraMatrix = initCameraMatrix2D(objectPoints, imagePoints[0], imageSize, 0);
	distCoeffs = Mat::zeros(8, 1, CV_64F);


	vector<vector<Point3f> > objectPoints(1);
	calcBoardCornerPositions(boardSize, squareSize, objectPoints[0]);
	objectPoints.resize(imagePoints.size(), objectPoints[0]);

	//Find intrinsic and extrinsic camera parameters
	double rms;
	vector<Mat> rvecs, tvecs;
	vector<float> reprojErrs;
	int calibFlags = CALIB_FIX_ASPECT_RATIO | CALIB_FIX_PRINCIPAL_POINT | CALIB_ZERO_TANGENT_DIST;
	rms = calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, rvecs, tvecs, calibFlags);

	printf("Done with RMS error = %f\n", rms);

	bool ok = checkRange(cameraMatrix) && checkRange(distCoeffs);


	double totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints, rvecs, tvecs, cameraMatrix, distCoeffs, reprojErrs);

	printf("Average epipolar err = %f\n", totalAvgErr);


	// save intrinsic parameters
	const string intrinsicsFn = fileName;
	FileStorage fs(intrinsicsFn, FileStorage::WRITE);
	if (fs.isOpened())
	{
		fs << "M1" << cameraMatrix << "D1" << distCoeffs;
		fs.release();

		printf("Intrinsic parameters successfully saved to '%s'\n", intrinsicsFn.c_str());
	}
	else
		cerr << "Error: can not save the intrinsic parameters\n";


	// COMPUTE AND DISPLAY RECTIFICATION
	if (!showRectified)
		return true;

	Mat map1, map2;
	//Precompute maps for cv::remap()
	initUndistortRectifyMap(cameraMatrix, distCoeffs, Mat(), getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, imageSize, 1, imageSize, 0), imageSize, CV_32F, map1, map2);

	Mat canvas;
	double sf;
	int w, h;

	sf = 300. / MAX(imageSize.width, imageSize.height);
	w = cvRound(imageSize.width*sf);
	h = cvRound(imageSize.height*sf);
	w = imageSize.width;
	h = imageSize.height;
	canvas.create(h, w, CV_8UC3);


	for (int i = 0; i < imageList.size(); i++)
	{
		Mat img = imread(imageList[i], 0), rimg, cimg;
		remap(img, rimg, map1, map2, INTER_LINEAR);
		cvtColor(rimg, cimg, COLOR_GRAY2BGR);
		Mat canvasPart = canvas(Rect(0, 0, w, h));
		resize(cimg, canvasPart, canvasPart.size(), 0, 0, INTER_AREA);

		//			if (useCalibrated)
		//			{
		//				Rect vroi(cvRound(validRoi[k].x*sf), cvRound(validRoi[k].y*sf),
		//					cvRound(validRoi[k].width*sf), cvRound(validRoi[k].height*sf));
		//				rectangle(canvasPart, vroi, Scalar(0, 0, 255), 3, 8);
		//			}


		for (int j = 0; j < canvas.rows; j += 16)
			line(canvas, Point(0, j), Point(canvas.cols, j), Scalar(0, 255, 0), 1, 8);

		imshow("rectified", canvas);
		char c = (char)waitKey();
		if (c == 27 || c == 'q' || c == 'Q')
			break;
	}

	return true;
}