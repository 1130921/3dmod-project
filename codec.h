#ifndef CODEC_H
#define CODEC_H

#include <vector>
#include <opencv2/opencv.hpp>


// Base class for all encoders
class Encoder {
    public:
        Encoder(unsigned int screenCols, unsigned int screenRows) : N(0),screenCols(screenCols), screenRows(screenRows){}
        unsigned int getNPatterns(){return N;}

        // Encoding
        virtual cv::Mat getEncodingPattern(unsigned int depth) = 0;
        virtual ~Encoder(){}
    protected:
        unsigned int N;
        unsigned int screenCols, screenRows;
};

class Decoder {
    public:
        Decoder(unsigned int screenCols, unsigned int screenRows) : N(0), screenCols(screenCols), screenRows(screenRows){}
        unsigned int getNPatterns(){return N;}

        // Decoding
        virtual void setFrame(unsigned int depth, const cv::Mat frame) = 0;
        virtual void decodeFrames(cv::Mat &up, cv::Mat &vp, cv::Mat &mask) = 0;
        virtual ~Decoder(){}
    protected:
        unsigned int N;
        unsigned int screenCols, screenRows;
};

#endif // CODEC_H
