#include "CodecGrayCode.h"
#include <cmath>
#include <iomanip>

//#include "cvtools.h"

static unsigned int Nhorz = 10;
static unsigned int Nvert = 10;

#ifndef log2f
#define log2f(x) (log(x)/log(2.0))
#endif

using namespace std;
using namespace cv;

/*
 * The purpose of this function is to convert an unsigned
 * binary number to reflected binary Gray code.
 *
 * The operator >> is shift right. The operator ^ is exclusive or.
 * Source: http://en.wikipedia.org/wiki/Gray_code
 */
static unsigned int binaryToGray(unsigned int num) {
    return (num >> 1) ^ num;
}

/*
 * Function takes the decimal number
 * Function takes the Nth bit (1 to 31)
 * Return the value of Nth bit from decimal
 * Source: http://icfun.blogspot.com/2009/04/get-n-th-bit-value-of-any-integer.html
 */
static int get_bit(int decimal, int N){

    // Shifting the 1 for N-1 bits
    int constant = 1 << (N-1);

    // If the bit is set, return 1
    if( decimal & constant ){
        return 1;
    }

    // If the bit is not set, return 0
    return 0;
}


// Encoder
EncoderGrayCode::EncoderGrayCode(unsigned int screenCols, unsigned int screenRows, unsigned int depth) : Encoder(screenCols, screenRows), depth(depth) {

    N = 0;

    // Set total pattern number
   
    this->N += depth * 2; // for each horizontal a normal pattern and qn inverse
    this->N += depth * 2; // for each horizontal a normal pattern and qn inverse

    // Encode every pixel column

	int NbitsHorz = ceilf(log2f((float)screenCols));

	// Number of vertical encoding patterns
	int NbitsVert = ceilf(log2f((float)screenRows));


    // Precompute horizontally encoding patterns
    for(unsigned int p=0; p<depth; p++){
        cv::Mat patternP(1, screenCols, CV_8UC3);
        // Loop through columns in first row
        for(unsigned int j=0; j<screenCols; j++){
            unsigned int jGray = binaryToGray(j);
            // Amplitude of channels
            float amp = get_bit(jGray, NbitsHorz-p);
            patternP.at<cv::Vec3b>(0,j) = cv::Vec3b(255.0*amp,255.0*amp,255.0*amp);
        }
        patterns.push_back(patternP);
		cv::Mat patternPInverse;
		cv::bitwise_not(patternP, patternPInverse);
		patterns.push_back(patternPInverse);
    }
    

    // Precompute vertical encoding patterns
    for(unsigned int p=0; p<depth; p++){
        cv::Mat patternP(screenRows, 1, CV_8UC3);

        // Loop through rows in first column
        for(unsigned int i=0; i<screenRows; i++){

            unsigned int iGray = binaryToGray(i);

            // Amplitude of channels
            float amp = get_bit(iGray, NbitsVert-p);
            patternP.at<cv::Vec3b>(i,0) = cv::Vec3b(255.0*amp,255.0*amp,255.0*amp);
        }
		patterns.push_back(patternP);
		cv::Mat patternPInverse;
		cv::bitwise_not(patternP, patternPInverse);
		patterns.push_back(patternPInverse);
    }

}

cv::Mat EncoderGrayCode::getEncodingPattern(unsigned int depth){
    return patterns[depth];
}

// Decoder
DecoderGrayCode::DecoderGrayCode(unsigned int screenCols, unsigned int screenRows, unsigned int depth) : Decoder(screenCols, screenRows), depth(depth){

    N = 0;

	this->N += depth * 2; // for each horizontal a normal pattern and an inverse
	this->N += depth * 2; // for each horizontal a normal pattern and an inverse

    frames.resize(N);

}

void DecoderGrayCode::setFrame(unsigned int depth, const cv::Mat frame){
    frames[depth] = frame;
}

void DecoderGrayCode::decodeFrames(cv::Mat &up, cv::Mat &vp, cv::Mat &mask){
	// TODO
}
